package br.com.italosilva.algoritmos.merge02;

import br.com.italosilva.algoritmos.domain.Clube;

public class IntercalaUmArray {

    public static void main(String[] args) {
        Clube[] grupoAD = {
                new Clube("Água Santa", 1),
                new Clube("Oeste", 3),
                new Clube("Ponte Preta", 6),
                new Clube("Santos", 7),
                new Clube("Ferroviária", 4),
                new Clube("Bragantino", 5),
                new Clube("Guarani", 7),
                new Clube("Corinthians", 7)
        };

        Clube[] classificacaoGeral = merge(grupoAD, 1 , 4 , grupoAD.length);

        for (Clube clube : classificacaoGeral){
            System.out.println(clube);
        }
    }

    private static Clube[] merge(Clube[] grupo, int inicio, int meio, int termino) {

        Clube[] classificacaoGeral = new Clube[termino-inicio];

        int atual = 0, atualA = inicio, atualB = meio;

        while(atualA < meio && atualB < termino) {

            Clube clubeA = grupo[atualA];
            Clube clubeB = grupo[atualB];

            if(clubeA.getPontos() < clubeB.getPontos()){
                classificacaoGeral[atual] = clubeA;
                atualA++;
            } else {
                classificacaoGeral[atual] = clubeB;
                atualB++;
            }
            atual++;
        }

        while(atualA < meio) {
            classificacaoGeral[atual] = grupo[atualA];
            atual++;
            atualA++;
        }

        while(atualB < meio) {
            classificacaoGeral[atual] = grupo[atualB];
            atual++;
            atualB++;
        }

        for(int contador = 0; contador < atual; contador++) {
            grupo[inicio+contador] = classificacaoGeral[contador];
        }

        return classificacaoGeral;
    }
}
