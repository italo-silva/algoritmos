package br.com.italosilva.algoritmos.buscabinaria;

import br.com.italosilva.algoritmos.domain.Clube;

public class BuscaBinaria {

    public static void main(String[] args) {

        Clube corinthians = new Clube("Corinthians", 7);

        Clube[] grupo = {
                new Clube("Santos", 7),
                new Clube("Ponte Preta", 6),
                new Clube("Oeste", 3),
                new Clube("Água Santa", 1),

                new Clube("Santo André", 9),
                new Clube("Novo Horizontino", 8),
                new Clube("Palmeiras", 7),
                new Clube("Botafogo", 1),

                new Clube("São Paulo", 8),
                new Clube("Inter de Limeira", 6),
                new Clube("Mirassol", 6),
                new Clube("Ituano", 2),

                new Clube("Guarani", 7),
                new Clube("Bragantino", 5),
                new Clube("Ferroviária", 4),
                corinthians
        };

        ordena(grupo, 0, grupo.length);

        int posicao = busca(grupo, 0, grupo.length, 9);

        System.out.println("O clube está na posição "+posicao);

        for (Clube clube: grupo) {
            System.out.println(clube.getNome()+" -> "+clube.getPontos());
        }
    }

    private static int busca(Clube[] grupo, int de, int ate, int buscando) {

        if (de > ate)
            return -1;

        int meio = (de + ate) / 2;

        Clube clubeCentro = grupo[meio];

        if(buscando == clubeCentro.getPontos())
            return meio;

        if(buscando < clubeCentro.getPontos())
            return busca(grupo, de, meio - 1, buscando);

        return busca(grupo, meio + 1, ate, buscando);
    }

    private static void ordena(Clube[] grupo, int de, int ate) {
        int elementos = ate-de;
        if(elementos > 1){
            int posicaoDoPivo = particiona(grupo, de , ate);
            ordena(grupo, de, posicaoDoPivo);
            ordena(grupo, posicaoDoPivo+1, ate);
        }

    }

    private static int particiona(Clube[] grupo, int inicio, int termino) {

        Clube pivo = grupo[termino - 1];
        int menoresEncontrados = 0;
        for(int analisando = 0; analisando < termino - 1; analisando++){
            Clube clube = grupo[analisando];
            if(clube.getPontos() <= pivo.getPontos()){
                troca(grupo, analisando, menoresEncontrados);
                menoresEncontrados++;
            }
        }

        troca(grupo, termino - 1, menoresEncontrados);

        return menoresEncontrados;
    }

    private static void troca(Clube[] grupo, int de, int para) {
        Clube clube1 = grupo[de];
        Clube clube2 = grupo[para];
        grupo[de] = clube2;
        grupo[para] = clube1;
    }
}
