package br.com.italosilva.algoritmos.merge01;

import br.com.italosilva.algoritmos.domain.Clube;

public class IntercalaDoisArrays {

    public static void main(String[] args) {

        Clube[] grupoA = {
                new Clube("Água Santa", 1),
                new Clube("Oeste", 3),
                new Clube("Ponte Preta", 6),
                new Clube("Santos", 7)
        };


        Clube[] grupoD = {
                new Clube("Ferroviária", 4),
                new Clube("Bragantino", 5),
                new Clube("Guarani", 7),
                new Clube("Corinthians", 7)
        };


        Clube[] classificacaoGeral = merge(grupoA, grupoD);

        for (Clube clube : classificacaoGeral){
            System.out.println(clube);
        }

    }

    private static Clube[] merge(Clube[] grupo1, Clube[] grupo2) {

        int quantidadeClubes = grupo1.length + grupo2.length;

        Clube[] classificacaoGeral = new Clube[quantidadeClubes];

        int atualA = 0, atualB = 0, atual = 0;

        while(atualA < grupo1.length && atualB < grupo2.length) {

            Clube clubeA = grupo1[atualA];
            Clube clubeB = grupo2[atualB];

            if(clubeA.getPontos() < clubeB.getPontos()){
                classificacaoGeral[atual] = clubeA;
                atualA++;
            } else {
                classificacaoGeral[atual] = clubeB;
                atualB++;
            }
            atual++;
        }

        while(atualA < grupo1.length) {
            classificacaoGeral[atual] = grupo1[atualA];
            atual++;
            atualA++;
        }

        while(atualB < grupo2.length) {
            classificacaoGeral[atual] = grupo2[atualB];
            atual++;
            atualB++;
        }

        return classificacaoGeral;
    }
}
