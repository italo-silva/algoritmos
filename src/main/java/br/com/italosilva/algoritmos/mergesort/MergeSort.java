package br.com.italosilva.algoritmos.mergesort;

import br.com.italosilva.algoritmos.domain.Clube;

public class MergeSort {

    public static void main(String[] args) {
        Clube[] grupo = {
                new Clube("Santos", 7),
                new Clube("Ponte Preta", 6),
                new Clube("Oeste", 3),
                new Clube("Água Santa", 1),

                new Clube("Santo André", 9),
                new Clube("Novo Horizontino", 8),
                new Clube("Palmeiras", 7),
                new Clube("Botafogo", 1),

                new Clube("São Paulo", 8),
                new Clube("Inter de Limeira", 6),
                new Clube("Mirassol", 6),
                new Clube("Ituano", 2),

                new Clube("Corinthians", 7),
                new Clube("Guarani", 7),
                new Clube("Bragantino", 5),
                new Clube("Ferroviária", 4)
        };

        ordena(grupo, 0, grupo.length);

        for (Clube clube : grupo){
            System.out.println(clube);
        }
    }

    private static void ordena(Clube[] grupo, int inicio, int fim) {

        int quantidade = fim - inicio;

        if(quantidade > 1) {
            int meio = (inicio + fim) / 2;
            ordena(grupo, inicio, meio);
            ordena(grupo, meio, fim);

            merge(grupo, inicio, meio, fim);
        }
    }

    private static void  merge(Clube[] grupo, int inicio, int meio, int termino) {

        Clube[] classificacaoGeral = new Clube[termino-inicio];

        int atual = 0, atualA = inicio, atualB = meio;

        while(atualA < meio && atualB < termino) {

            Clube clubeA = grupo[atualA];
            Clube clubeB = grupo[atualB];

            if(clubeA.getPontos() < clubeB.getPontos()){
                classificacaoGeral[atual] = clubeA;
                atualA++;
            } else {
                classificacaoGeral[atual] = clubeB;
                atualB++;
            }
            atual++;
        }

        while(atualA < meio) {
            classificacaoGeral[atual] = grupo[atualA];
            atual++;
            atualA++;
        }

        while(atualB < meio) {
            classificacaoGeral[atual] = grupo[atualB];
            atual++;
            atualB++;
        }

        for(int contador = 0; contador < atual; contador++) {
            grupo[inicio+contador] = classificacaoGeral[contador];
        }
    }
}
